﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public Transform player;
    // Use this for initialization

    public void EndLiveStart()
    {
        StartCoroutine(EndLive());
    }

    IEnumerator EndLive()
    {
        yield return new WaitForSecondsRealtime(3);
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(0);
    }

    // Update is called once per frame
    void Update()
    {
        if (player.position.x >= 2.2f && player.position.x <= 22.15f)
            transform.position = new Vector3(player.transform.position.x, 0, -20);
    }
}
