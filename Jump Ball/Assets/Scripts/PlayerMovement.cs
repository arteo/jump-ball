﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    public Camera camera;
    Rigidbody rigidbody;
    public Vector2 speedAndJumpForce;
    public GameObject firePacticle;
    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
       
        if (collision.gameObject.tag== "Danger")
        {
            camera.GetComponent<CameraMovement>().EndLiveStart();
            Instantiate(firePacticle,transform.position,new Quaternion(0,0,90,1));
            Destroy(gameObject);
        }
    }

 
    // Update is called once per frame
    void FixedUpdate()
    {
        if (Physics.Raycast(transform.position, -Vector3.up, 0.51f) && Input.GetAxis("Jump") != 0)
        {
            rigidbody.AddForce(new Vector3(0, speedAndJumpForce.y, 0), ForceMode.Impulse);
        }
        Vector2 speed = new Vector2();
        speed.x = speedAndJumpForce.x*Input.GetAxis("Horizontal");
        speed.y = rigidbody.velocity.y;
        rigidbody.velocity = speed;
    }


}
